#!/bin/bash -eu

FW_CODE="/usr/share/edk2/aarch64/QEMU_EFI-silent-pflash.raw"
FW_VARS_TEMPL="/usr/share/edk2/aarch64/QEMU_VARS.fd"

SIZE_64M=67108864

FW_CODE_RESIZED=$(mktemp)
cat "${FW_CODE}" > "${FW_CODE_RESIZED}"
truncate -s${SIZE_64M} "${FW_CODE_RESIZED}"

FW_VARS=$(mktemp)
cat "${FW_VARS_TEMPL}" > "${FW_VARS}"
truncate -s${SIZE_64M} "${FW_VARS}"

trap "rm -f ${FW_CODE_RESIZED} ${FW_VARS}" EXIT

/usr/bin/qemu-system-aarch64 \
    -machine virt \
    -machine accel=kvm:tcg -m 512 -boot menu=off \
    -cpu max \
    -blockdev node-name=code,driver=file,filename="${FW_CODE_RESIZED}",read-only=on \
    -blockdev node-name=vars,driver=file,filename="${FW_VARS}" \
    -machine pflash0=code \
    -machine pflash1=vars \
    -serial stdio \
    -monitor none \
    -nographic \
    -net none \
    -kernel "${1}"
